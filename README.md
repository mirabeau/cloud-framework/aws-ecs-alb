AWS ECS ALB
=========
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create ALBs with Cloudformation. AWS resources that will be created are:
 * ElasticLoadbalancer V2
 * Securitygroup for the ALB
 * SecurityGroup Ingress rule allowing ALB to connect to provided ECS cluster SG

A test playbook has been supplied which rolls out the lambda function included with this role.
You can run this playbook using the following command:
```bash
ansible-playbook aws-ecs-alb/tests/test.yml --inventory aws-ecs-alb/tests/inventory.yml
```
This command should run outside of the role dir and requires all dependant roles to be installed in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
 * aws-iam
 * aws-vpc
 * aws-securitygroups
 * aws-ecs-cluster

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
--------------
```yaml
---
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"

aws_ecs_alb_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  logs_bucket_name: "{{ account_name }}-logs"
  albs:
    - role                  : "ecs-alb"
      alb_scheme            : "internal"  # Default ALB scheme is 'internal' (alternative is internet-facing)
      ecs_cluster_stack_name: "{{ aws_ecs_cluster_params.cluster_name }}"
      custom_subnets        : ""  # Optional parameter; use your own subnets if you don't want to use the default subnets created by the aws-vpc role
      listeners             :
        "80": {}
        #--------------------------------------
        # Example ssl configuration
        #--------------------------------------
        # "443":
        #   ssl            : 1
        #   certificate_arn: "<your ssl cert arn>"
        #--------------------------------------
        # Optional - ssl listener extra certs:
        #--------------------------------------
        #   extra_certificate_arns:
        #     - "<your extra ssl cert arn 1>"
        #     - "<your extra ssl cert arn 2>"
```

Example Playbook
----------------
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"

    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1

    aws_utils_params:
      ecs_encrypt_ami: True

  pre_tasks:
    - name: Get latest AWS AMI's
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis

  roles:
    - aws-iam
    - aws-vpc
    - aws-securitygroups
    - aws-ecs-cluster
    - aws-ecs-alb
```
License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
